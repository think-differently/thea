[![pipeline status](https://gitlab.com/think-differently/thea/badges/master/pipeline.svg)](https://gitlab.com/think-differently/thea/commits/master)
# Thea
Thea is an open-source browser built with Python 3. Thea is Greek for view.

## Required modules
Required modules that come with Python 3 are:
* re
* os
* sys
* io

Other required modules (install with `python -m pip install ___ --user`) are:
* requests (should come with python3, may need pip install)
* emoji
* emoji_unicode
* lxml (Metatrepo dependency)
* kivy
* docutils
* pygments
* pypiwin32
* rfc3987

## Installing Dependencies
While the dependencies are mentioned in the [Required modules](#Required modules) section above, it is recommended to use the following command to be sure you have all the needed dependencies:
```
python -m pip install --upgrade pip wheel setuptools

python -m pip install requests lxml emoji emoji_unicode docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.angle
python -m pip install kivy.deps.gstreamer
python -m pip install kivy
```

## Cloning
Use the following commands to properly and fully clone this project:
```
git clone https://gitlab.com/think-differently/thea.git
```

To update the entire project, use the following command:
```
git pull
```

To push any changes to the branch you are on, use the following commands:
```
git add .
git commit -m "A summary of the changes made"
git push
```
## Compiling
Use [`pyinstaller`](http://www.pyinstaller.org/) to package Thea into executables for Windows, Linux, macOS, and other operating systems.
* Install using `python -m pip install pyinstaller`.
* To package, go to the directory of Thea and use the command `pyinstaller thea.py`.

The file `.gitlab-ci.yml` builds it to make sure it is buildable. See that file for a Linux example of how to build it.