from tkinter import ttk
from tkinter import *
from tkinter.scrolledtext import ScrolledText
import requests
import metatrepo
import emoji
import emoji_unicode
from provoli import provoli
import os
import sys
import re
# import shutil
# import tempfile
from PIL import Image, ImageTk  # Pillow


class program(object):
    def __init__(self, root, **kwargs):
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            self.base_path = sys._MEIPASS
        except Exception:
            self.base_path = os.path.abspath(".")
        self.emoji_path = self.base_path + "\\emojis"
        self.root = provoli.App(root, title="Thea", icon=self.base_path+'\\thea.ico', bg='lightgray', titlebg='gray', titlefg='black', state='zoomed', zoom=True, minimize=True, i_height=800, i_width=1000)
        self.images = {
            'home'   : Image.open(self.emoji_path+'\\black\\F0011.png'),
            'refresh': Image.open(self.emoji_path+'\\black\\1F504.png'),
            'back'   : Image.open(self.emoji_path+'\\black\\F0000.png'),
            'forward': Image.open(self.emoji_path+'\\black\\F0000.png').rotate(180),
            'cancel' : Image.open(self.emoji_path+'\\black\\F0001.png'),
            'newTab' : Image.open(self.emoji_path+'\\black\\F0001.png').rotate(45),
        }
        self.headers = {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0 Thea/0.0.0.1'}
        self.taskbarHeight = 30
        self.tabIconSize = (16,16)
        self.canvas = Canvas(self.root.content, width=self.root.winfo_width(), height=self.root.winfo_height())
        self.canvas.pack()
        self.taskbar = Frame(self.root.content, width=self.root.winfo_width(), height=self.taskbarHeight, bg="white")
        self.canvas.create_window(0, 0, window=self.taskbar, anchor=NW, height=self.taskbarHeight, width=self.root.winfo_width())
        # self.taskbar.grid(row=0, column=0, sticky="W")

        #navigation buttons
        self.bH = 24 #nav button height
        self.backBtnImg = ImageTk.PhotoImage(self.images['back'].resize((self.bH,self.bH), Image.ANTIALIAS))
        self.forwardBtnImg = ImageTk.PhotoImage(self.images['forward'].resize((self.bH,self.bH), Image.ANTIALIAS))
        self.refreshBtnImg = ImageTk.PhotoImage(self.images['refresh'].resize((self.bH,self.bH), Image.BICUBIC))
        self.homeBtnImg = ImageTk.PhotoImage(self.images['home'].resize((self.bH,self.bH), Image.ANTIALIAS))
        self.cancelBtnImg = ImageTk.PhotoImage(self.images['cancel'].resize((self.bH,self.bH), Image.ANTIALIAS))
        self.newTabBtnImg = ImageTk.PhotoImage(self.images['newTab'].resize(self.tabIconSize, Image.ANTIALIAS))

        self.backButton = Button(self.taskbar, image=self.backBtnImg, height=self.bH, width=self.bH)
        self.backButton.grid(row=0, column=0, sticky="W")
        self.forwardButton = Button(self.taskbar, image=self.forwardBtnImg, height=self.bH, width=self.bH)
        self.forwardButton.grid(row=0, column=1, sticky="W")
        self.refreshButton = Button(self.taskbar, image=self.refreshBtnImg, height=self.bH, width=self.bH, command=self.browseURL)
        self.refreshButton.grid(row=0, column=2, sticky="W")
        self.homeButton = Button(self.taskbar, image=self.homeBtnImg, height=self.bH, width=self.bH)
        self.homeButton.grid(row=0, column=3, sticky="W")
        # emoji_unicode.replace(u'🏠', lambda e: u'<img src="{filename}.png" alt="{raw}">'.format(filename=e.code_points, raw=e.unicode))
        #address bar
        self.URLBar = Entry(self.taskbar, width=(self.root.winfo_width() - (self.bH * 4)))
        self.URLBar.grid(row=0, column=4, sticky="W")

        #tabs
        self.currentTab = 0
        self.icon = []
        self.tabs = []
        self.root.update()
        self.nb = ttk.Notebook(self.root.content, width=self.root.winfo_width(), height=self.root.winfo_height()-self.taskbarHeight)
        self.canvas.create_window(0, self.taskbarHeight, window=self.nb, anchor=NW, height=self.root.winfo_height()-self.taskbarHeight, width=self.root.winfo_width())
        # self.nb.grid(row=1, column=0, sticky="W")
        self.tabLength = 0
        self.newTab(title="New Tab", pos=0, icon="thea.ico")
        self.nb.enable_traversal()
        self.nb.add(Frame(self.nb), image=self.newTabBtnImg, sticky="E")

        #bindings
        self.URLBar.bind("<Return>", self.browseURL)
        self.root.bind("<Control-t>", self.newTab)
        self.nb.bind("<ButtonRelease-1>", self.tabClicked)
        self.root.bind("<Control-w>", self.closeTab)
        self.root.bind("<Control-Shift-q>", self.root.destroy)
        self.root.mainloop()

    def browseURL(self, event=None):
        self.refreshButton.config(image=self.cancelBtnImg)
        self.root.update()
        self.refreshButton.grid(row=0, column=2, sticky="W")
        print(self.currentTab)
        URL = self.URLBar.get()
        pattern = re.compile('.*?://')
        if not pattern.match(URL):
            URL = 'https://'+URL
            self.URLBar.delete(0, END)
            self.URLBar.insert(0, URL)
        self.tabs[self.currentTab]["URL"] = URL
        try:
            self.req = requests.get(self.tabs[self.currentTab]["URL"], headers=self.headers)
            print("Content:", self.req._content)
            print("\nContent Consumed:", self.req._content_consumed)
            print("\nNext:", self.req._next)
            print("\nStatus Code:", self.req.status_code)
            print("\nHeaders:", self.req.headers)
            print("\nRaw:", self.req.raw)
            print("\nURL:", self.req.url)
            print("\nEncoding:", self.req.encoding)
            print("\nHistory:", self.req.history)
            print("\nReason:", self.req.reason)
            print("\nCookies:", self.req.cookies)
            print("\nElapsed:", self.req.elapsed)
            print("\nRequest:", self.req.request)
            print("\nConnection:", self.req.connection)
            self.tabs[self.currentTab]["data"] = self.req._content
            self.tabs[self.currentTab]["frame"].config(state=NORMAL)
            self.tabs[self.currentTab]["frame"].delete(1.0, END)
            self.tabs[self.currentTab]["frame"].insert(END, self.tabs[self.currentTab]["data"])
            self.tabs[self.currentTab]["frame"].config(state=DISABLED)
            self.parseHTML(self.tabs[self.currentTab]["data"])
        except:
            print(sys.exc_info()[0].__dict__)
        self.refreshButton.config(image = self.refreshBtnImg)
        self.root.update()
    
    def parseHTML(self, code):
        parser = metatrepo.HTML_Renderer()
        print(parser.feed(code))

    def tabClicked(self, event=None, x=0, y=0):
        clicked_tab = self.nb.tk.call(self.nb._w, "identify", "tab", x, y)
        print(clicked_tab)

    def changeTab(self, pos, event=None):
        self.currentTab = pos
        # print(self.currentTab)

    def newTab(self, event=None, title="New Tab", pos=None, icon='thea.ico'):
        if pos == None:
            pos = self.tabLength
        self.pos = pos
        self.currentTab = self.pos
        self.tabLength += 1
        self.tabs.append({
            'tab': Frame(self.nb),
            'title': title,
            'pos': pos,
            'icon': Image.open(icon).resize(self.tabIconSize),
            'data': ""
        })
        self.nb.add(self.tabs[self.pos]["tab"], text=self.tabs[self.pos]["title"], image=ImageTk.PhotoImage(self.tabs[self.pos]["icon"]), compound=LEFT, sticky="NW")
        self.tabs[self.pos]["frame"] = ScrolledText(self.tabs[self.pos]["tab"], wrap=WORD, state=DISABLED, width=self.root.winfo_width(), height=self.root.winfo_height()-(self.taskbarHeight * 3))
        # self.tabs[self.pos]["frame"].tag_add()
        self.tabs[self.pos]["frame"].pack(expand=1, fill="both", pady=(0,self.bH))
        self.tabs[self.pos]["id"] = self.nb.select()
        #self.tabs[self.pos]["frame"].insert(END, "🏠")
        print(self.tabs)
    
    def closeTab(self, event=None, pos=None):
        if pos == None:
            pos = self.currentTab
        self.nb.forget(self.tabs[self.pos]["id"])

if __name__ == '__main__':
    root = Tk()
    app = program(root)
