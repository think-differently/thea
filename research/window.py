import math, cmath, re, array
from urllib.parse import quote, quote_plus, unquote, unquote_plus
from slimit.lexer import Lexer
import dukpy
interpreter = dukpy.JSInterpreter()
# interpreter.evaljs()
# https://www.valentinog.com/blog/engines/
class window:
	def __init__(self):
		self.Infinity = math.inf #cmath.inf
		self.NaN = math.nan #cmath.nan
		self.null = None
		self.undefined = None
		self.globalThis = self
	def alert(self, *args):
		print(args)
		# Find a way to create a Kivy popup.
		# Something restricted to the tab and not blocking the browser.
	# def eval(self, *args):
		# Once we have a working lexer, we can use this as a reference to it
		# args would be used an input for the lexer, which is then converted to Python and run as such
	def isNaN(self, x):
		return math.isnan(x)
		# return cmath.isnan(x)
	def isFinite(self, x):
		return math.isfinite(x)
		# return cmath.isfinite(x)
	def parseFloat(self, string):
		try:
			return float(string)
		except:
			return math.nan
	def parseInt(self, string):
		try:
			return int(string)
		except:
			return math.nan
	def decodeURI(self, uri):
		return unquote(uri)
	def decodeURIComponent(self, uri):
		return unquote(uri)
	def encodeURI(self, uri):
		return quote(uri, safe="-_.!~*'();,/?:@&=+$#")
	def encodeURIComponent(self, uri):
		return quote(uri, safe="-_.!~*'()")
	class Math:
		def __init__(self):
			# Math class
			self.E = math.e #cmath.e # is in JavaScript global.Math
			self.PI = math.pi #cmath.pi # is in JavaScript global.Math
			self.LN2 = math.log(2) # is in JavaScript global.Math
			self.LN10 = math.log(10) # is in JavaScript global.Math
			self.LOG2E = math.log(math.e, 2) # is in JavaScript global.Math
			self.LOG10E = math.log10(math.e) # is in JavaScript global.Math
			self.SQRT1_2 = math.sqrt(1/2) # is in JavaScript global.Math
			self.SQRT2 = math.sqrt(2) # is in JavaScript global.Math
			self.TAU = math.tau #cmath.tau
			self.INFJ = cmath.infj
			self.NaNJ = cmath.nanj
		def abs(self, x):
			# is in JavaScript global.Math
			return abs(x)
		def acos(self, x):
			# is in JavaScript global.Math
			return math.acos(x)
			# return cmath.acos(x)
		def acosh(self, x):
			# is in JavaScript global.Math
			return math.acosh(x)
			# return cmath.acosh(x)
		def asin(self, x):
			# is in JavaScript global.Math
			return math.asin(x)
			# return cmath.asin(x)
		def asinh(self, x):
			# is in JavaScript global.Math
			return math.asinh(x)
			# return cmath.asinh(x)
		def atan(self, x):
			# is in JavaScript global.Math
			return math.atan(x)
			# return cmath.atan(x)
		def atan2(self, y, x):
			# is in JavaScript global.Math
			return math.atan2(y, x)
		def atanh(self, x):
			# is in JavaScript global.Math
			return math.atanh(x)
			# return cmath.atanh(x)
		def cbrt(self, x):
			# is in JavaScript global.Math
			return math.pow(x, 1/3)
			# inspired by StackOverflow user
		def ceil(self, x):
			# is in JavaScript global.Math
			return math.ceil(x)
		def clz32(self, x):
			# is in JavaScript global.Math
			n = 32
			y = x >> 16
			if (y != 0):
				n = n - 16
				x = y
			y = x >> 8
			if (y != 0):
				n = n - 8
				x = y
			y = x >> 4
			if (y != 0):
				n = n - 4
				x = y
			y = x >> 2
			if (y != 0):
				n = n - 2
				x = y
			y = x >> 1
			if (y != 0):
				return n - 2
			return n - x
			# from GeeksForGeeks
		def cos(self, x):
			# is in JavaScript global.Math
			return math.cos(x)
			# return cmath.cos(x)
		def cosh(self, x):
			# is in JavaScript global.Math
			return math.cosh(x)
			# return cmath.cosh(x)
		def degrees(self, x):
			return math.degrees(x)
		def erf(self, x):
			return math.erf(x)
		def erfc(self, x):
			return math.erfc(x)
		def exp(self, x):
			# is in JavaScript global.Math
			return math.exp(x)
			# return cmath.exp(x)
		def expm1(self, x):
			# is in JavaScript global.Math
			return math.expm1(x)
		def fabs(self, x):
			return math.fabs(x)
		def factorial(self, x):
			return math.factorial(x)
		def floor(self, x):
			# is in JavaScript global.Math
			return math.floor(x)
		def fmod(self, x, y):
			return math.fmod(x, y)
		def frexp(self, x):
			return math.frexp(x)
		def fround(self, x):
			farray = array.array('f')
			farray.append(float(x))
			return farray.tolist()[0]
			# is in JavaScript global.Math
		def fsum(self, iterable):
			return math.fsum(iterable)
		def gamma(self, x):
			return math.gamma(x)
		def gcd(self, a, b):
			return math.gcd(a, b)
		def hypot(self, x, y):
			# is in JavaScript global.Math
			return math.hypot(x, y)
		def imul(self, x, y):
			# is in JavaScript global.Math
			return x * y
			# If this is not accurate for Math.imul, then this needs some revising
		def isclose(self, a, b, *args, rel_tol=1e-09, abs_tol=0.0):
			return math.isclose(a, b, args, rel_tol, abs_tol)
			# return cmath.exp(a, b, args, rel_tol, abs_tol)
		def isfinite(self, x):
			return math.isfinite(x)
			# return cmath.isfinite(x)
		def isinf(self, x):
			return math.isinf(x)
			# return cmath.isinf(x)
		def ldexp(self, x, i):
			return math.ldexp(x, i)
		def lgamma(self, x):
			return math.lgamma(x)
		def log(self, x, base=math.e):
			# is in JavaScript global.Math
			return math.log(x, base)
			# return cmath.log(x, base)
		def log1p(self, x):
			# is in JavaScript global.Math
			return math.log1p(x)
		def log2(self, x):
			# is in JavaScript global.Math
			return math.log2(x)
		def log10(self, x):
			# is in JavaScript global.Math
			return math.log10(x)
			# return cmath.log10(x)
		def max(self, *args):
			# is in JavaScript global.Math
			return max(args)
		def min(self, *args):
			# is in JavaScript global.Math
			return min(args)
		def modf(self, x):
			return math.modf(x)
		def phase(self, x):
			return cmath.phase(x)
		def polar(self, x):
			return cmath.polar(x)
		def pow(self, x):
			# is in JavaScript global.Math
			return math.pow(x)
		def radians(self, x):
			return math.radians(x)
		def rect(self, r, phi):
			return cmath.rect(r, phi)
		def remainder(self, x, y):
			return math.remainder(x, y)
		def sign(self, x):
			# is in JavaScript global.Math
			return math.copysign(1, x)
		def sin(self, x):
			# is in JavaScript global.Math
			return math.sin(x)
			# return cmath.sin(x)
		def sinh(self, x):
			# is in JavaScript global.Math
			return math.sinh(x)
			# return cmath.sinh(x)
		def sqrt(self, x):
			# is in JavaScript global.Math
			return math.sqrt(x)
			# return cmath.sqrt(x)
		def tan(self, x):
			# is in JavaScript global.Math
			return math.tan(x)
			# return cmath.tan(x)
		def tanh(self, x):
			# is in JavaScript global.Math
			return math.tanh(x)
			# return cmath.tanh(x)
		def trunc(self, x):
			# is in JavaScript global.Math
			return math.trunc(x)
class jsLexer:
	def __init__(self):
		self.lexer = Lexer()
	def input(self, input):
		self.lexer.input(input)
		return self.lexer
	def getNext(self):
		token = self.lexer.token()
		return [token.type, token.value, token.lineno, token.lexpos]