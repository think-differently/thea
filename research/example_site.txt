<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<title>The Gathering of Ocala</title>


<link rel="icon" type="image/x-icon" sizes="192x192" href="../img/favicon.png">

<link rel="apple-touch-icon" href="../img/favicon.png">

<meta name="msapplication-square310x310logo" content="../img/favicon.png">
<link rel="apple-touch-startup-image" href="../img/logo_white_bg.png">



<link rel="stylesheet" href="../css/pages.css">
<link rel="manifest" href="../manifest.webmanifest">
<link rel="canonical" href="https://site.thegatheringocala.com/">

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta id="view" name="viewport" content=", initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=1, viewport-fit=cover">
<meta name="description" content="We are a community of Jesus-followers who walk this journey called life embracing our God in true worship, embracing one another through fellowship and discipleship, and embracing our community through sharing the tangible love of Jesus!">
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="The Gathering of Ocala">
<meta name="twitter:description" content="We are a community of Jesus-followers who walk this journey called life embracing our God in true worship, embracing one another through fellowship and discipleship, and embracing our community through sharing the tangible love of Jesus!">
<meta name="twitter:image:src" content="../img/logo_small_transparent.PNG">
<meta property="og:description" content="We are a community of Jesus-followers who walk this journey called life embracing our God in true worship, embracing one another through fellowship and discipleship, and embracing our community through sharing the tangible love of Jesus!">
<meta property="og:title" content="The Gathering of Ocala">
<meta property="og:image" content="../img/logo_small_transparent.PNG">
<meta name="theme-color" content="#1c1a1a" />
</head>
<body>
<script src='https://d33wubrfki0l68.cloudfront.net/js/ed30e4e20c1598a5c8603a065b37447f46aa9f82/js/jquery-3.4.0.min.js' type="b05906fa9cc28f275d7b8403-text/javascript"></script>
<script type="b05906fa9cc28f275d7b8403-text/javascript">
		const alertErrors = false;
      if (alertErrors === true) {
        $(window).on("error", function(evt) {
            alert("jQuery error event:", evt);
            var e = evt.originalEvent; // get the javascript event
            alert("original event:", e);
            if (e.message) { 
                alert("Error:\n\t" + e.message + "\nLine:\n\t" + e.lineno + "\nFile:\n\t" + e.filename);
            } else {
                alert("Error:\n\t" + e.type + "\nElement:\n\t" + (e.srcElement || e.target));
            }
        });
        $(window).on("warn", function(evt) {
            alert("jQuery warn event:", evt);
            var e = evt.originalEvent; // get the javascript event
            alert("original event:", e);
            if (e.message) { 
                alert("Warning:\n\t" + e.message + "\nLine:\n\t" + e.lineno + "\nFile:\n\t" + e.filename);
            } else {
                alert("Warning:\n\t" + e.type + "\nElement:\n\t" + (e.srcElement || e.target));
            }
        });
      }
	</script>
<script async src='https://d33wubrfki0l68.cloudfront.net/js/dc5fc39bc777f21d2a950c98834e5dd6c35bc0b0/js/modernizr-custom.js' type="b05906fa9cc28f275d7b8403-text/javascript"></script>
<script src='https://d33wubrfki0l68.cloudfront.net/js/bce2aeb8e85c5f5e246501edc988c2558938ffb5/js/jquery.smoothstate.js' type="b05906fa9cc28f275d7b8403-text/javascript"></script>
<div id="bg">
<div class="bg 1"></div>
<div class="bg 2"></div>
<div class="bg 3"></div>
<div class="bg 4"></div>
</div>
<div id="main">

<div class="site page-home">
<script type="b05906fa9cc28f275d7b8403-text/javascript">
				$('html').removeClass('no-js');
			</script>
<main>
<article><div id="logo"><a href="/"><script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="b05906fa9cc28f275d7b8403-|49"></script><img src="https://d33wubrfki0l68.cloudfront.net/d8a8406af162f15b9af65deec95d48783576cf28/99005/webp/logo_transparent-glow.webp" onerror="this.onerror=null; this.src='https://d33wubrfki0l68.cloudfront.net/a4e99dad6b7181e687475cdc51f9ef13d39ff6ef/a6bf4/img/logo_transparent-glow.png'" alt="The Gathering of Ocala" id="logo-image" width=1600 height=500></a></div>
<div id="slogan">Loving God. Loving People. No Matter What.</div>
<nav class="desktop-nav">
<ul>
<li class="dropdown hover">
<input type="checkbox" id="d1" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#d2&quot;).prop(&quot;checked&quot;, false);$(&quot;#d3&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />
<button class="dropbtn">Connect
<i></i></button>
<ul class="dropdown-content">
<li><a href="../location/">Location</a>
</li>
<li><a href="../times/">Times</a></li>
<li><a href="../projects/">Projects</a>
</li>
<li><a href="../pray/">Pray</a></li>
<li><a href="../contact/">Contact Us</a>
</li>
</ul>
</li>
<li class="dropdown hover">
<input type="checkbox" id="d2" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#d1&quot;).prop(&quot;checked&quot;, false);$(&quot;#d3&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />
<button class="dropbtn">Discover
<i></i></button>
<ul class="dropdown-content">
<li><a href="../about/">About Us</a></li>
<li><a href="../beliefs/">What We
Believe</a></li>
<li><a href="../pastors-and-staff/">Pastors and Staff</a></li>
<li><a href="../reaching-out/">Reaching
Out</a></li>
</ul>
</li>
<li class="dropdown hover">
<input type="checkbox" id="d3" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#d1&quot;).prop(&quot;checked&quot;, false);$(&quot;#d2&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />
<button class="dropbtn">Give
<i></i></button>
<ul class="dropdown-content">
<li><a href="../give/">Online</a></li>
<li><a href="../mail/">Mail</a></li>
<li><a href="../in-kind/">In-Kind</a></li>
</ul>
</li>
</ul>
</nav>



<nav class="mobile-nav">
<div id="menuToggle" class="shadowed">

<input type="checkbox" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#m1&quot;).prop(&quot;checked&quot;, false);$(&quot;#m2&quot;).prop(&quot;checked&quot;, false);$(&quot;#m3&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />

<span></span>
<span></span>
<span></span>
<ul id="menu">
<li class="dropdown hover">
<input type="checkbox" id="m1" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#m2&quot;).prop(&quot;checked&quot;, false);$(&quot;#m3&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />
<button class="dropbtn">Connect
<i></i></button>
<ul class="dropdown-content">
<li><a href="../location/">Location</a>
</li>
<li><a href="../times/">Times</a></li>
<li><a href="../projects/">Projects</a>
</li>
<li><a href="../pray/">Pray</a></li>
<li><a href="../contact/">Contact Us</a>
</li>
</ul>
</li>
<li class="dropdown hover">
<input type="checkbox" id="m2" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#m1&quot;).prop(&quot;checked&quot;, false);$(&quot;#m3&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />
<button class="dropbtn">Discover
<i></i></button>
<ul class="dropdown-content">
<li><a href="../about/">About Us</a></li>
<li><a href="../beliefs/">What We
Believe</a></li>
<li><a href="../pastors-and-staff/">Pastors and Staff</a></li>
<li><a href="../reaching-out/">Reaching Out</a></li>
</ul>
</li>
<li class="dropdown hover">
<input type="checkbox" id="m3" onclick="if (!window.__cfRLUnblockHandlers) return false; $(&quot;#m1&quot;).prop(&quot;checked&quot;, false);$(&quot;#m2&quot;).prop(&quot;checked&quot;, false);" data-cf-modified-b05906fa9cc28f275d7b8403-="" />
<button class="dropbtn">Give
<i></i></button>
<ul class="dropdown-content">
<li><a href="../give">Online</a></li>
<li><a href="../mail">Mail</a></li>
<li><a href="../in-kind">In-Kind</a>
</li>
</ul>
</li>
</ul>
</div>
</nav>
</article>
</main>
</div>


<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133709856-1" type="b05906fa9cc28f275d7b8403-text/javascript"></script>
</div>
<script type="b05906fa9cc28f275d7b8403-text/javascript">
		$(function(){
          'use strict';
          var $page = $('#main'),
              options = {
                debug: true,
                prefetch: true,
                cacheLength: 2,
                forms: 'form',
                onStart: {
                  duration: 250, // Duration of our animation
                  render: function ($container) {
                    // Add your CSS animation reversing class
                    $container.addClass('is-exiting');
                    // Restart your animation
                    smoothState.restartCSSAnimations();
                  }
                },
                onReady: {
                  duration: 0,
                  render: function ($container, $newContent) {
                    // Remove your CSS animation reversing class
                    $container.removeClass('is-exiting');
                    // Inject the new content
                    $container.html($newContent);
                  }
                }
              },
              smoothState = $page.smoothState(options).data('smoothState');

        });
  </script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="b05906fa9cc28f275d7b8403-|49" defer=""></script></body>
</html>
